//KUSH SHAH CS610 1262 PRP

import java.util.*;
import java.io.*;
import static java.lang.Math.*;

public class pgrk1262 {
		String file;
	
        static int itns,init_value,vertices,edges;
        static int[] out_links;
        static int[][] L; 
        final static double damp = 0.85;

		static double rate_err = 0.00001;
        static double[] Source_pgrk;
        static double offset;
        pgrk1262(int itns, int init_value, String file)
        {
        this.itns = itns;
        this.init_value = init_value;
        this.file = file;
        int p = 0;
        int q = 0;
        try {  
        	
            Scanner scanner = new Scanner(new File(file));
            vertices = scanner.nextInt();
            edges = scanner.nextInt();
            offset = (1-damp)/vertices;
            L = new int[vertices][vertices];
            for(int i = 0; i < vertices; i++)
             for(int j = 0; j < vertices; j++)
               L[i][j] = 0;
            Source_pgrk = new double[vertices];
            while(scanner.hasNextInt())
            {
                L[scanner.nextInt()][scanner.nextInt()] = 1; 
            }
            
            out_links = new int[vertices];
            for(int i = 0; i < vertices; i++) {
            	out_links[i] = 0;
                for(int j = 0; j < vertices; j++) {
                	out_links[i] += L[i][j];
                }
            }
          
            switch(init_value) {
            case 0:
              for(int i = 0; i < vertices; i++) {
                Source_pgrk[i] = 0;
              }
              break;
            case 1:
              for(int i = 0; i < vertices; i++) {
                Source_pgrk[i] = 1;
              }
              break;
            case -1:
              for(int i =0; i < vertices; i++) {
                Source_pgrk[i] = 1.0/vertices;
              }
              break;
            case -2:
              for(int i =0; i < vertices; i++) {
                Source_pgrk[i] = 1.0/Math.sqrt(vertices);
              }
              break;
            }
            scanner.close();
        }
        catch(FileNotFoundException exxe)
        {
        	System.out.println("File input Error, check log :");
        	exxe.printStackTrace();
        }
    }
    public static void main(String[] args)
    {
        if(args.length != 3) {
            System.out.println("Invalid number of Args : Enter \n java pgrk1262 <Iterations> <Initial_value> <Filename>");
            return;
        }
        int iterations = Integer.parseInt(args[0]);
        int initialvalue = Integer.parseInt(args[1]);
        String file = args[2];
        if( !(initialvalue >= -2 && initialvalue <= 1) ) {
            System.out.println("Enter -2, -1, 0 or 1 for initialvalue");
            return;
        }
if(iterations < 0){
	rate_err = Math.pow(10, iterations);
	iterations = 10;
	System.out.println();
}
        pgrk1262 pr = new pgrk1262(iterations, initialvalue, file);
        double[] D = new double[vertices];
        boolean flag = true;
         if(vertices > 10) {
            itns = 0;
            for(int i =0; i < vertices; i++) {
              Source_pgrk[i] = 1.0/vertices;
            }
            int i = 0;
          do {
               if(flag == true)
               {
                  flag = false;
               }
               else
               {
                 for(int l = 0; l < vertices; l++) {
                   Source_pgrk[l] = D[l];
                 }
               }
               for(int l = 0; l < vertices; l++) {
                 D[l] = 0.0;
               }

               for(int j = 0; j < vertices; j++) {
                 for(int k = 0; k < vertices; k++)
                 {
                    if(L[k][j] == 1) {
                        D [j] += Source_pgrk[k]/out_links[k];
                    }
                 }
               }
               for(int l = 0; l < vertices; l++) {
                 D[l] = damp*D[l] + offset;
               }
               i++;
             } while (checkConverge1262(Source_pgrk, D) != true);
             System.out.println("Iter: " + i);
             for(int l = 0 ; l < vertices; l++) {
                 System.out.printf("P[" + l + "] = %.7f\n",Math.round(D[l]*10000000.0)/10000000.0);
             }
             return;
        }
         System.out.printf("Base Case : 0");
        for(int j = 0; j < vertices; j++) {
            System.out.printf(" :P[" + j + "]=%.7f",Math.round(Source_pgrk[j]*10000000.0)/10000000.0);
        }
        
        if (itns != 0) {
          for(int i = 0; i < itns; i++)
          {
        	  System.out.println();
              for(int l = 0; l < vertices; l++) {
                D[l] = 0.0;
              }
            
              for(int j = 0; j < vertices; j++) {
                for(int k = 0; k < vertices; k++)
                {
                    if(L[k][j] == 1) {
                        D[j] += Source_pgrk[k]/out_links[k];
                    } 
                }
              }
              System.out.printf("Iteration : " + (i+1));
              for(int l = 0; l < vertices; l++) {
                D[l] = damp*D[l] + offset;
                System.out.printf(" :P[" + l + "]=%.7f",Math.round(D[l]*10000000.0)/10000000.0);
              }
            
              for(int l = 0; l < vertices; l++) {
                Source_pgrk[l] = D[l]; 
              } 
        }
          System.out.println();
        }
        else 
        { 
          int i = 0;
          do {
               if(flag == true)
               {
                  flag = false;
               }
               else
               {
                 for(int l = 0; l < vertices; l++) {
                   Source_pgrk[l] = D[l];
                 }
               }
               for(int l = 0; l < vertices; l++) {
                 D[l] = 0.0;
               }

               for(int j = 0; j < vertices; j++) {
                 for(int k = 0; k < vertices; k++)
                 {
                    if(L[k][j] == 1) {
                        D[j] += Source_pgrk[k]/out_links[k];
                    }
                 }
               } 
               System.out.print("Iter    : " + (i+1));
               for(int l = 0; l < vertices; l++) {
                 D[l] = damp*D[l] + offset;
                 System.out.printf(" :P[" + l + "]=%.6f",Math.round(D[l]*1000000.0)/1000000.0);
               }
               i++;  
             } while (checkConverge1262(Source_pgrk, D) != true);  
        System.out.println(); 
        }
    }
    static boolean checkConverge1262(double[] src, double[] target)
    {
        for(int i = 0; i < vertices; i++)
        {
          if ( abs(src[i] - target[i]) > rate_err )
            return false;
        }
        return true;
    }
}