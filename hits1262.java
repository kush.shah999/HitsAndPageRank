//KUSH SHAH CS610 1262 PRP

import java.util.*;
import java.io.*;
import static java.lang.Math.*;

public class hits1262 {

        int iterations;
        int initval;
        String filename;
        int vertices;      
        int edges;      
        int[][] adjc_mtrx;   
        double[] hub_arr;
        double[] auth_arr;
        final double errorrate = 0.00001; 
    hits1262(int iter, int initval, String filename)     
    {
        this.iterations= iter;
        this.initval = initval;
        this.filename = filename;
        try {        
            Scanner scanner = new Scanner(new File(filename));
            vertices = scanner.nextInt();
            edges = scanner.nextInt();
            adjc_mtrx= new int[vertices][vertices];
            for(int i = 0; i < vertices; i++)
             for(int j = 0; j < vertices; j++)
            	 adjc_mtrx[i][j] = 0;

            while(scanner.hasNextInt())
            {
            	adjc_mtrx[scanner.nextInt()][scanner.nextInt()] = 1; 
            }

            hub_arr = new double[vertices];
            auth_arr = new double[vertices];
            switch(initval) {
            case 0:
              for(int i = 0; i < vertices; i++) {
                hub_arr[i] = 0;
                auth_arr[i] = 0;
              }
              break;
            case 1:
              for(int i = 0; i < vertices; i++) {
                hub_arr[i] = 1;
                auth_arr[i] = 1;
              }
              break;
            case -1:
              for(int i =0; i < vertices; i++) {
                hub_arr[i] = 1.0/vertices;
                auth_arr[i] = 1.0/vertices;
              }
              break;
            case -2:
              for(int i =0; i < vertices; i++) {
                hub_arr[i] = 1.0/Math.sqrt(vertices);
                auth_arr[i] = 1.0/Math.sqrt(vertices);
              }
              break;
            }
scanner.close();
        }
        catch(FileNotFoundException exxe)
        {
        	exxe.printStackTrace();
        }
    }

    public static void main(String[] args)
    {
        if(args.length != 3) {
            System.out.println("Incorrect input format. Use : \n java hits1262 <iterations> <initialvalue> <filename>");
            return;
        }
        int iterations = Integer.parseInt(args[0]);
        int initialvalue = Integer.parseInt(args[1]);
        String filename = args[2];
        if (iterations>10){
        	iterations = 10;
        }

        if( !(initialvalue >= -2 && initialvalue <= 1) ) {
            System.out.println("Enter -2, -1, 0 or 1 for initialvalue");
            return;
        }

        hits1262 ht = new hits1262(iterations, initialvalue, filename);

        ht.hitsAlgo1262();
    }
 
  
    public void hitsAlgo1262()
    {
        double[] h = new double[vertices];
        double[] a = new double[vertices];
        double a_scale_factor = 0.0;
        double a_sum_square = 0.0;
        double h_scale_factor = 0.0;
        double h_sum_square = 0.0; 
        double[] aprev = new double[vertices]; 
        double[] hprev = new double[vertices]; 

        if(vertices > 10) {
            iterations = 0;
            for(int i =0; i < vertices; i++) {
                h[i] = 1.0/vertices;
                a[i] = 1.0/vertices;
                hprev[i] = h[i];
                aprev[i] = a[i];
            }
            
          int i = 0;
          do {  
               for(int r = 0; r < vertices; r++) {
                   aprev[r] = a[r];
                   hprev[r] = h[r];
               }
                for(int p = 0; p < vertices; p++) {
                    a[p] = 0.0;
                }
            
                for(int j = 0; j < vertices; j++) {
                    for(int k = 0; k < vertices; k++) {
                        if(adjc_mtrx[k][j] == 1) {
                            a[j] += h[k]; 
                        }
                    }
                }
                for(int p = 0; p < vertices; p++) {
                    h[p] = 0.0;
                }

                for(int j = 0; j < vertices; j++) {
                    for(int k = 0; k < vertices; k++) {
                        if(adjc_mtrx[j][k] == 1) {
                            h[j] += a[k]; 
                        }
                    }
                }
                a_scale_factor = 0.0;
                a_sum_square = 0.0;
                for(int l = 0; l < vertices; l++) {
                    a_sum_square += a[l]*a[l];    
                }
                a_scale_factor = Math.sqrt(a_sum_square); 
                for(int l = 0; l < vertices; l++) {
                    a[l] = a[l]/a_scale_factor;
                }
                h_scale_factor = 0.0;
                h_sum_square = 0.0;
                for(int l = 0; l < vertices; l++) {
                    h_sum_square += h[l]*h[l];    
                }
                h_scale_factor = Math.sqrt(h_sum_square); 
                for(int l = 0; l < vertices; l++) {
                    h[l] = h[l]/h_scale_factor;
                }
                i++; 
          } while( false == checkConverge1262(a, aprev) || false == checkConverge1262(h, hprev));
          System.out.println("Iter:   " + i);
          for(int l = 0; l < vertices; l++) {
              System.out.printf(" A/H[%d]=%.7f/%.7f \n",l,Math.round(a[l]*10000000.0)/10000000.0,Math.round(h[l]*10000000.0)/10000000.0); 
          }
          return;
        }
        for(int i = 0; i < vertices; i++)
        {
            h[i] = hub_arr[i];
            a[i] = auth_arr[i];
            hprev[i] = h[i];
            aprev[i] = a[i]; 
        }
        System.out.print("Base:    0 :");
        for(int i = 0; i < vertices; i++) {
          System.out.printf(" A/H[%d]=%.7f/%.7f",i,Math.round(auth_arr[i]*10000000.0)/10000000.0,Math.round(hub_arr[i]*10000000.0)/10000000.0); 
        }
        
        if (iterations != 0) { 
            for(int i = 0; i < iterations; i++) { 
                for(int p = 0; p < vertices; p++) {
                    a[p] = 0.0;
                }
            
                for(int j = 0; j < vertices; j++) {
                    for(int k = 0; k < vertices; k++) {
                        if(adjc_mtrx[k][j] == 1) {
                            a[j] += h[k]; 
                        }
                    }
                }
                for(int p = 0; p < vertices; p++) {
                    h[p] = 0.0;
                }

                for(int j = 0; j < vertices; j++) {
                    for(int k = 0; k < vertices; k++) {
                        if(adjc_mtrx[j][k] == 1) {
                            h[j] += a[k]; 
                        }
                    }
                }
                a_scale_factor = 0.0;
                a_sum_square = 0.0;
                for(int l = 0; l < vertices; l++) {
                    a_sum_square += a[l]*a[l];    
                }
                a_scale_factor = Math.sqrt(a_sum_square); 
                for(int l = 0; l < vertices; l++) {
                    a[l] = a[l]/a_scale_factor;
                }
                h_scale_factor = 0.0;
                h_sum_square = 0.0;
                for(int l = 0; l < vertices; l++) {
                    h_sum_square += h[l]*h[l];    
                }
                h_scale_factor = Math.sqrt(h_sum_square); 
                for(int l = 0; l < vertices; l++) {
                    h[l] = h[l]/h_scale_factor;
                } 
                System.out.println();
                System.out.print("Iter:    " + (i+1) + " :");
                for(int l = 0; l < vertices; l++) {
                    System.out.printf(" A/H[%d]=%.7f/%.7f",l,Math.round(a[l]*10000000.0)/10000000.0,Math.round(h[l]*10000000.0)/10000000.0); 
                }
   
            }
        }
        else
        {
          int i = 0;
          do {  
                for(int r = 0; r < vertices; r++) {
                    aprev[r] = a[r];
                    hprev[r] = h[r];
                }

                for(int p = 0; p < vertices; p++) {
                    a[p] = 0.0;
                }
            
                for(int j = 0; j < vertices; j++) {
                    for(int k = 0; k < vertices; k++) {
                        if(adjc_mtrx[k][j] == 1) {
                            a[j] += h[k]; 
                        }
                    }
                }
                for(int p = 0; p < vertices; p++) {
                    h[p] = 0.0;
                }

                for(int j = 0; j < vertices; j++) {
                    for(int k = 0; k < vertices; k++) {
                        if(adjc_mtrx[j][k] == 1) {
                            h[j] += a[k]; 
                        }
                    }
                }
                a_scale_factor = 0.0;
                a_sum_square = 0.0;
                for(int l = 0; l < vertices; l++) {
                    a_sum_square += a[l]*a[l];    
                }
                a_scale_factor = Math.sqrt(a_sum_square); 
                for(int l = 0; l < vertices; l++) {
                    a[l] = a[l]/a_scale_factor;
                }
                h_scale_factor = 0.0;
                h_sum_square = 0.0;
                for(int l = 0; l < vertices; l++) {
                    h_sum_square += h[l]*h[l];    
                }
                h_scale_factor = Math.sqrt(h_sum_square); 
                for(int l = 0; l < vertices; l++) {
                    h[l] = h[l]/h_scale_factor;
                }
                i++; 
                System.out.println();
                System.out.print("Iter:    " + i + " :");
                for(int l = 0; l < vertices; l++) {
                    System.out.printf(" A/H[%d]=%.7f/%.7f",l,Math.round(a[l]*10000000.0)/10000000.0,Math.round(h[l]*10000000.0)/10000000.0); 
                }
          } while( false == checkConverge1262(a, aprev) || false == checkConverge1262(h, hprev));
        }
        System.out.println();
    }

boolean checkConverge1262(double[] p, double[] q)
{
   for(int i = 0 ; i < vertices; i++) {
       if ( abs(p[i] - q[i]) > errorrate ) 
         return false;
   }
   return true;
} 
}